<?php

namespace TestBundle\Controller;

use TestBundle\Entity\Commandes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class CommandesController extends Controller
{

  public function indexAction(Request $request){

      $commande = new Commandes();

      // On ajoute les champs de l'entité que l'on veut à notre formulaire
      $form = $this->get('form.factory')->createBuilder('form', $commande)
      ->add('valeur', 'text')
      ->add('save', 'submit')
      ->getForm();

      $form->handleRequest($request);

      if ($form->isValid()) {
     // On enregistre notre objet $commande dans la base de données
     $em = $this->getDoctrine()->getManager();
     $em->persist($commande);
     $em->flush();
   }
      // On passe la méthode createView() du formulaire à la vue afin qu'elle puisse afficher le formulaire toute seule
      return $this->render('TestBundle:Default:index.html.twig', array('form' => $form->createView()));
    }

}
